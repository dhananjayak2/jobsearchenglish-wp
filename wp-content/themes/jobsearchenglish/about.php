<?php 

/*Template Name: About */
wp_head();
get_header(); ?>

   <!-- Blog Details Section Begin -->
    <section class="blog-details spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-details-inner">
                        <div class="blog-detail-title">
                            <h2>About Us</h2>

                        </div>
                        <!-- <div class="blog-large-pic">
                            <img src="img/blog/blog-detail.jpg" alt="">
                        </div> -->
                        <div class="blog-detail-desc">
                            <p><?php the_field('about_content'); ?>
                            </p>
                        </div>
                        <div class="blog-quote">
                            <p> <?php the_field('ab_quote'); ?></p>
                        </div>
                        <div class="blog-more">
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="<?php the_field('ab_image_1'); ?>" alt="">
                                </div>
                                <div class="col-sm-4">
                                    <img src="<?php the_field('ab_image_2'); ?>" alt="">
                                </div>
                                <div class="col-sm-4">
                                    <img src="<?php the_field('ab_image_3'); ?>" alt="">
                                </div>
                            </div>
                        </div>
                        <p><?php the_field('about_content_2'); ?></p>




                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Details Section End -->



    <?php get_footer();
 wp_footer();
 ?>