<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="JOBSEARCH website">
    <meta name="keywords" content="JOBSEARCH, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>JOB_SEARCH_ENGLISH</title>

    <link rel="icon" href="<?php echo get_template_directory_uri() ?>/img/favi.png" type="image/gif" sizes="64x64">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;600;900&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/bae552976e.js" crossorigin="anonymous"></script>
    <!-- Css Styles -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css" type="text/css">
    <!-- <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"> -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/responsive.css" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header-section">
        <!-- <div class="header-top">
            <div class="container">
                <div class="ht-left">
                    <div class="mail-service">
                        <i class=" fa fa-envelope"></i> hello.jobsearchenglish@gmail.com
                    </div>
                    <div class="phone-service">
                        <i class=" fa fa-phone"></i> +65 11.188.888
                    </div>
                </div>
                <div class="ht-right">
                    <a href="#" class="login-panel"><i class="fa fa-user"></i>Login</a>
                    <div class="lan-selector">
                        <select class="language_drop" name="countries" id="countries" style="width:300px;">
                            <option value='yt' data-image="img/flag-1.jpg" data-imagecss="flag yt"
                                data-title="English">English</option>
                            <option value='yu' data-image="img/flag-2.jpg" data-imagecss="flag yu"
                                data-title="Bangladesh">German </option>
                        </select>
                    </div>
                    <div class="top-social">
                        <a href="#"><i class="ti-facebook"></i></a>
                        <a href="#"><i class="ti-twitter-alt"></i></a>
                        <a href="#"><i class="ti-linkedin"></i></a>
                        <a href="#"><i class="ti-pinterest"></i></a>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="container">
            <div class="inner-header">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        <div class="logo">
                            <?php if ( is_active_sidebar( 'part1' ) ) : ?>
                        <?php dynamic_sidebar( 'part1' );
                            ?>
                        <?php endif;
                                ?>
                            
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7">
                        <nav class="navbar navbar-expand-lg navbar-light ">

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                              <span class="navbar-toggler-icon"></span>
                          </button>

                          <div class="collapse navbar-collapse" id="navbarSupportedContent">



                            <?php

                            wp_nav_menu(array('theme_location' => 'top-menu', 
                                'add_li_class' => 'nav-item', 
                                'container' => 'ul',
                                'menu_class' => 'navbar-nav mr-auto main-nav', 
                                'menu_id' => FALSE,));

                                ?>



                                <form class="search-container" role="search" method="get"  action="<?php echo esc_url(home_url('/')); ?>">

                                    <input id="search-bar" type="search"  class="search-field" placeholder="" value="<?php echo get_search_query(); ?>" name="s" />

                                    <button type="submit" class="search-submit"><span class="screen-reader-text"><i class="fas fa-search"></i></span></button>
                                    <!-- <a type="submit" class="search-submit" href="#"><i class="fas fa-search"></i></a> -->
                                </form>






                            </div>
                        </nav>
                    </div>
                    <div class="col-lg-3 text-right col-md-3">
                        <ul class="nav-right">
                            <li class="log-btn">
                                <a href="login.html" class="primary-btn">Login</a>
                            </li>
                            <li class="reg-btn">
                                <a href="register.html" class="primary-btn">Register</a>

                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-item">
         <?php
         include 'menu.php';
         ?>

     </div>
 </header>
    <!-- Header End -->