<?php get_header(); ?>



    <section class="probootstrap_cover_v3 overflow-hidden relative text-center" id="section-home">

        <div class="overlay"></div>
        <div class="head_img">
           <!-- <img src="<?php echo get_template_directory_uri(); ?>/assets/images/inner-img/img2.jpg"> !-->
        </div>


        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-md topic">
                    <h2 class="heading mb-2 probootstrap-animate">Search result</h2>


                </div> 
            </div>
        </div>
        <!-- <div class="scroll-wrap js-scroll-wrap probootstrap-animate">
          <a href="#section-feature" class="text-white probootstrap_font-24 smoothscroll"><i class="ion-chevron-down"></i></a>
        </div> -->
    </section>
    <!-- END section -->


    <section class="probootstrap_section" id="section-feature">
        <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-md-6 text-center mb-5 probootstrap-animate">
                    <!-- <h2>The Features</h2> -->
                    <?php
                    if (!empty( $q['s'])) {
    $search = $this->parse_search( $q );
    
    echo "234".$search;
}
                    
                    
                    ?>
                    
                    
                    <?php if (have_posts()) : ?>
                        <p><?php printf(__('<b>Search Results for: %s</b>', ''), '<span><b>' . get_search_query() . '</b></span>'); ?></p>
                    <?php else : ?>
                        <p class="page-title"><?php _e('Nothing Found', ''); ?></p>
                    <?php endif; ?>


                </div>
            </div>


            <?php if (have_posts()) : ?>

                <?php while (have_posts()) : the_post(); ?>





                    <div class="row">
                        <div class="col-12 single-result ">

                            <div class="col-md-12 col-xs-12 result-content">
                                <h5><?php the_title(); ?></h5>
                                <p><?php
                                    echo wp_trim_words(get_the_content(), 40, '...');
                                    ?><a href="<?php the_permalink() ?>"> Read More ..</a></p>

                            </div>
                        </div>
                    </div>

  

                <?php endwhile; ?>



            <?php endif; ?>





        </div>
    </section>
    <!-- END section -->
    <style >
        .col-12.single-result {
            display: flex;
        }
    </style>







    <!-- END section -->


    <?php get_footer(); ?>
    <?php wp_footer(); ?>



</body>
</html>