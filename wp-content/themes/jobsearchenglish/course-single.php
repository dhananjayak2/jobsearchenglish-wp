<?php
 
/*Template Name: courses-single  */

 get_header();?>


  
    <!-- Hero Section Begin -->
    <section class="hero-section all-course">

    </section>
    <!-- Hero Section End -->

    <!-- Latest Blog Section Begin -->
    <section class="latest-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 d-flex justify-content-around">
                    <a href="#" class="primary-btn">LEVEL01</a>
                    <a href="#" class="primary-btn">LEVEL 02</a>
                    <a href="#" class="primary-btn">LEVEL 03</a>
                </div>
            </div>


        </div>
    </section>

    <section class="latest-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-md-12 all-quiz">
                    <p class="Quizzes">Quizzes</p>
                    <div class="col-md-6 quiz">


                        <div class="quiz-bg">
                            <div class="quiz-img">
                                <img src="img/quiz/korean.png" alt="korean">
                            </div>
                            <div class="quiz-detail">
                                <a class="quizz" href="single-question.html">Culture</a>
                                <p><span>ssWritten By: </span>P. Brown</p>
                                <p><span>Description: </span>A secret is something that you do not ..</p>
                                <a class="read-more" href="#">read more</a>
                                <p class="level-top">LEVEL 01</p>
                            </div>

                        </div>

                        <div class="quiz-bg">
                            <div class="quiz-img">
                                <img src="img/quiz/korean.png" alt="korean">
                            </div>
                            <div class="quiz-detail">
                                <a class="quizz" href="single-question.html">Grammar</a>
                                <p><span>Written By: </span>P. Brown</p>
                                <p><span>Description: </span>A secret is something that you do not ..</p>
                                <a class="read-more" href="#">read more</a>
                                <p class="level-top">LEVEL 01</p>
                            </div>

                        </div>

                        <div class="quiz-bg">
                            <div class="quiz-img">
                                <img src="img/quiz/korean.png" alt="korean">
                            </div>
                            <div class="quiz-detail">
                                <a class="quizz" href="single-question.html">Sports</a>
                                <p><span>Written By: </span>P. Brown</p>
                                <p><span>Description: </span>A secret is something that you do not ..</p>
                                <a class="read-more" href="#">read more</a>
                                <p class="level-top">LEVEL 01</p>
                            </div>

                        </div>

                    </div>
                    <div class="col-md-6 quiz">


                        <div class="quiz-bg">
                            <div class="quiz-img">
                                <img src="img/quiz/korean.png" alt="korean">
                            </div>
                            <div class="quiz-detail">
                                <a class="quizz" href="single-question.html">Jokes</a>
                                <p><span>Written By: </span>P. Brown</p>
                                <p><span>Description: </span>A secret is something that you do not ..</p>
                                <a class="read-more" href="#">read more</a>
                                <p class="level-top">LEVEL 01</p>
                            </div>

                        </div>

                        <div class="quiz-bg">
                            <div class="quiz-img">
                                <img src="img/quiz/korean.png" alt="korean">
                            </div>
                            <div class="quiz-detail">
                                <a class="quizz" href="single-question.html">Phrases</a>
                                <p><span>Written By: </span>P. Brown</p>
                                <p><span>Description: </span>A secret is something that you do not ..</p>
                                <a class="read-more" href="#">read more</a>
                                <p class="level-top">LEVEL 01</p>
                            </div>

                        </div>

                        <div class="quiz-bg">
                            <div class="quiz-img">
                                <img src="img/quiz/korean.png" alt="korean">
                            </div>
                            <div class="quiz-detail">
                                <a class="quizz" href="single-question.html">Quotes</a>
                                <p><span>Written By: </span>P. Brown</p>
                                <p><span>Description: </span>A secret is something that you do not ..</p>
                                <a class="read-more" href="#">read more</a>
                                <p class="level-top">LEVEL 01</p>
                            </div>

                        </div>

                    </div>
                    <div class="col-md-12 pagination-botom d-flex justify-content-center">
                        <nav aria-label="Page navigation example ">
                            <ul class="pagination pagi-bottom">
                                <li class="page-item"><a class="page-link" href="#">Prev</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
    </section>




<?php get_footer(); ?>

  