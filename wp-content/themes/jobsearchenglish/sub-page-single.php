<?php
 

/*Template Name: sub-page-single  */

 get_header();?>

    <!-- Hero Section Begin -->
    <section class="hero-section all-course">

    </section>
    <!-- Hero Section End -->

    <!-- Latest Blog Section Begin -->
    <section class="latest-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 d-flex justify-content-around">
                    <a href="levels.html" class="primary-btn">LEVEL 01</a>
                    <a href="levels.html" class="primary-btn">LEVEL 02</a>
                    <a href="levels.html" class="primary-btn">LEVEL 03</a>
                </div>
            </div>


        </div>
    </section>

    <!-- Latest Blog Section Begin -->
    

    <section class="latest-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-md-12 all-quiz">
                    <div class="col-md-8 quiz">
                        <p class="Quizzes">Quizzes</p>
<?php

        $args_cont_dig = array('order' => 'ASC', 'category' => '12', 'taxonomy'               => 'LEVEL 01','posts_per_page' => 12);

        ?>

        <?php $args_contdig_item = get_posts($args_cont_dig); ?>

        <?php if (have_posts()) : ?>

            <?php foreach ($args_contdig_item as $post) : setup_postdata($post); ?>



                <?php

                $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');

                ?>

                         <div class="quiz-bg">
                            <div class="quiz-img">
                                <img src="<?php echo $featured_img_url; ?>" alt="korean">
                            </div>
                            <div class="quiz-detail">
                                <a class="quizz" href="<?php the_permalink() ?>"><?php the_title(); ?></a>

                                <p><span>Written By: </span><?php the_field('single view_written_by'); ?></p>
                                <p><span>Description: </span><?php echo substr(strip_tags($post->post_content), 0, 50); ?></p>
                                <a href="<?php the_permalink() ?>">read more</a>
                                <p class="level-top"><?php echo $args_cont_dig['taxonomy']; ?></p>
                            </div>

                        </div>
<?php endforeach; ?>

            <?php wp_reset_postdata(); ?>

        <?php endif; ?>
                    </div>
                    <div class="col-md-4 side-ads"></div>
                    <img class="side-img" src="<?php echo get_template_directory_uri() ?>/img/side-ad.png">
                </div>
                <div class="col-md-12 pagination-botom d-flex justify-content-center">
                    <nav aria-label="Page navigation example ">
                        <ul class="pagination pagi-bottom">
                            <li class="page-item"><a class="page-link" href="#">Prev</a></li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>

    <?php get_footer();?>