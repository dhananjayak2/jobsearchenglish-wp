<?php 
function register_my_menus() {
    register_nav_menus(
            array(
                'primary' => __('Primary Menu'),
                'top-menu' => __('Top menu'),
                'footer-menu' => __('Footer- Quick Links'),
                'footer-menu-2' => __('Footer- Legal Stuff'),
                
    ));
}
add_action('init', 'register_my_menus');

add_filter( 'nav_menu_link_attributes', function($atts) {
        $atts['class'] = "k2-main-menu";
        return $atts;
}, 100, 1 );




function theme_widgets_init() {
    register_sidebar(array(
        'name' => 'Main Logo',
        'id' => 'part1',
        'before_widget' => '<span>',
        'after_widget' => '</span>',
   
    ));
 register_sidebar(array(
        'name' => 'Footer Logo',
        'id' => 'part2',
        'before_widget' => '<span>',
        'after_widget' => '</span>',
   
    ));
register_sidebar(array(
        'name' => 'Footer Content',
        'id' => 'part3',
        'before_widget' => '<span>',
        'after_widget' => '</span>',
   
    ));
register_sidebar(array(
        'name' => 'Footer social medai',
        'id' => 'part4',
        'before_widget' => '<span>',
        'after_widget' => '</span>',
   
    ));


    }

add_action('widgets_init', 'theme_widgets_init');
add_theme_support( 'post-thumbnails' );