<?php

/*Template Name: all-course  */

get_header();?>


<!-- Hero Section Begin -->
<section class="hero-section all-course">

</section>
<!-- Hero Section End -->

<!-- Latest Blog Section Begin -->
<section class="latest-blog spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>All Courses</h2>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <?php if ( have_rows( 'all-course') ): ?>
                <?php while( have_rows( 'all-course') ): the_row();?>


                    <div class="course-item col-lg-4 col-md-6">
                        <div class="course-img">
                            <img src="<?php the_sub_field('course_image'); ?>" alt="course01">
                            <div class="single-latest-course">

                                <div class="latest-text">

                                    <a href="#">
                                        <h4><?php the_sub_field('course_name'); ?></h4>
                                    </a>
                                    <p><?php the_sub_field('course_details'); ?></p>
                                    <p class="courese-detail"><?php the_sub_field('course_description'); ?></p>
                                </div>
                            </div>
                        </div>
                        <a class="course-link" href="<?php the_sub_field('course_link'); ?>"><span></span></a>
                    </div>

                <?php endwhile;?>
            <?php endif;?>


        </div>

    </div>
</section>
<!-- Latest Blog Section End -->








<?php get_footer();?>