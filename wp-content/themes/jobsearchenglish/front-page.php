<?php
 

/*Template Name: front-page  */

 get_header();?>


<section class="hero-section">
        <div class="hero-items owl-carousel">
            <div class="single-hero-items set-bg" data-setbg="<?php echo get_template_directory_uri() ?>/img/hero-1.jpg">
                <div class="container">
                <?php $hero = get_field('hero1');?>
                    <div class="row">
                        <div class="col-lg-8">
                            <span><?php echo $hero['small_tytle'];?></span>
                            <h1><?php echo $hero['main_title'];?></h1>
                            <p><?php echo $hero['paragraph'];?></p>
                            <a href="<?php echo $hero['link01'];?>" class="primary-btn"><?php echo $hero['link_text01'];?></a>
                        </div>
                    </div>
                    <!-- <div class="off-card">
                        <h2>Sale <span>50%</span></h2>
                    </div> -->
                </div>
            </div>
            <div class="single-hero-items set-bg" data-setbg="<?php echo get_template_directory_uri() ?>/img/hero-2.jpg">
                <div class="container">
                <?php $hero = get_field('hero1_copy');?>
                    <div class="row">
                        <div class="col-lg-8">
                        <span><?php echo $hero['small_tytle'];?></span>
                            <h1><?php echo $hero['main_title'];?></h1>
                            <p><?php echo $hero['paragraph'];?></p>
                            <a href="<?php echo $hero['link01'];?>" class="primary-btn"><?php echo $hero['link_text01'];?></a>
                        </div>
                    </div>
                    <!-- <div class="off-card">
                        <h2>Sale <span>50%</span></h2>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Latest Blog Section Begin -->
    <section class="latest-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Our Courses</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
<?php if ( have_rows( 'all-course',398 ) ): ?>
<?php  $i = 0;?>

    

            <?php while( have_rows( 'all-course',398 ) ): the_row();?>
<?php $i++; ?>
        <?php if( $i > 6 ): ?>
            <?php break; ?>
        <?php endif; ?>
                <div class="course-item col-lg-4 col-md-6">
                    <div class="course-img">
                        <img src="<?php the_sub_field('course_image'); ?>" alt="course01">
                        <div class="single-latest-course">

                            <div class="latest-text">

                                <a href="#">
                                    <h4><?php the_sub_field('course_name'); ?></h4>
                                </a>
                        <p><?php the_sub_field('course_details'); ?></p>
                     <p class="courese-detail"><?php the_sub_field('course_description'); ?></p>
                            </div>
                        </div>
                    </div>
                    <a class="course-link" href="<?php the_sub_field('course_link'); ?>"><span></span></a>
                </div>

               <?php endwhile;
               else :?>
            <?php endif;?>


            </div>
            <a href="http://localhost/jobsearchenglish-wp/all-course/" class="primary-btn">All Courses</a>
        </div>
    </section>
    <!-- Latest Blog Section End -->



    <!-- comment Banner Section Begin -->
    <section class="comment-area spad">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title comment-title">
                        <h2>What People Says About Us?</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit .
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-12">

                    <div class="comment-slider owl-carousel">

<?php if ( have_rows( 'testimonial' ) ): ?>
            <?php while( have_rows( 'testimonial' ) ): the_row();?>




                        <div class="product-item">
                            <img class="coma" src="<?php echo get_template_directory_uri() ?>/img/comments/coma.png">
                            <div class="pi-pic">
                                <img src="<?php the_sub_field('image_Testimonial'); ?>" alt="">



                            </div>
                            <div class="pi-text">

                                <a href="#">
                                    <h5><?php the_sub_field('testimonial_content'); ?></h5>
                                </a>
                                <div class="comment-name">
                                    <p class="comment-person"><?php the_sub_field('testimonial-name'); ?></p>
                                    <p class="comment-possition"><?php the_sub_field('testimonial-position'); ?></p>

                                </div>
                            </div>
                        </div>

                       <?php endwhile;?>
            <?php endif;?> 

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Women Banner Section End -->
    <section class="follow-s">
        <div class="container">
            <div class="row">
                <div class="col-md-12 follow-us">
                    <div class="follow-topic">
                        <h2>Follow Us</h2>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit volup tatem .</p>
                    </div>
                    <form class="send-emai" action="/action_page.php">

                        <input class="name-mail" type="text" id="fname" name="fname" value="" placeholder="name">

                        <input class="name-mail" type="text" id="lname" name="lname" value="" placeholder="email">
                        <button class="submit-btn01" type="submit" form="form1" value="Submit">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </section>


    <section class="team">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Meet Our Crew</h2>
                        <p><?php the_field('Content_meat_our_crew'); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- Instagram Section Begin -->
                <div class="col-md-12 ">


<?php if ( have_rows( 'profiles' ) ): ?>
            <?php while( have_rows( 'profiles' ) ): the_row();?>

                    <div class="col-md-4 team-item">
                        <div class="team-photo">
                            <div class="inside-text">

                                <p> <?php the_sub_field('lecture_details'); ?></p>
                            </div>
                            <img src="<?php the_sub_field('lecture_image'); ?>">
                        </div>
                        <div class="team-name">
                            <h4><?php the_sub_field('lecture_name'); ?></h4>
                            <p><?php the_sub_field('lecture_qualification'); ?></p>
                        </div>
                    </div>
                    
<?php endwhile;?>
            <?php endif;?>

                </div>
            </div>
        </div>
        <!-- Instagram Section End -->
    </section>

    <?php 
    wp_footer();
    get_footer();?>