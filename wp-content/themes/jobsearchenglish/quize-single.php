<?php 
/* Template Name: quize-single * Template Post Type: post*/ /*The template for displaying full width single posts. */

 get_header();?>


<?php
           $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
           ?>
    <!-- Latest Blog Section Begin -->
    <section class="latest-blog spad">
        <div class="container">
            <div class="row">
                <img class="top-ad" src="<?php echo get_template_directory_uri() ?>/img/ad-top.png">
            </div>


        </div>
    </section>

    <section class="latest-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-md-12 all-quiz">

                    <div class="col-md-8 single-quiz">


                        <div class="quiz-bg-single">
                            <div class="quiz-topic">
                                <h4><?php the_title(); ?></h4>
                            </div>
                            <div class="quiz-img-single">
                                <img src="<?php echo $featured_img_url; ?>">
                            </div>
                            <div class="queation">
                                <h5><?php the_field('single view_sub_title'); ?></h5>
                                <p><span>Written By: </span><?php the_field('single view_written_by'); ?></p>
                                <p class="queation-detail"><?php echo substr(strip_tags($post->post_content), 0, 5000); ?>
                                </p>
                            </div>
                        </div>
                        <p class="Quizzes">Quizzes</p>
                        <?php echo do_shortcode('[qsm quiz=2]'); ?>
                      
                      <?php the_field('single_view_quize'); ?>
                        

                        <!-- <a href="#" class="primary-btn question-submit-btn">SUBMIT</a> -->



                    </div>
                    <div class="col-md-4 side-ads "></div>
                    <img class="side-img " src="<?php echo get_template_directory_uri() ?>/img/side-ad.png ">
                </div>

            </div>
        </div>
    </section>






<?php get_footer(); ?>