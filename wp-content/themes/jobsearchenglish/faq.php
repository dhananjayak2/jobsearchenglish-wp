
<?php 

/*Template Name: Faq  */

get_header(); ?>


<!-- Faq Section Begin -->
<div class="faq-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="faq-accordin">
                    <div class="accordion" id="accordionExample">
                        <div class="card">

                            <div class="card-heading active">
                                <a class="active" data-toggle="collapse" data-target="#collapseOne">
                                 <?php the_field('fqa_section_1'); ?>
                             </a>
                         </div>
                         <div id="collapseOne" class="collapse show" data-parent="#accordionExample">
                            <div class="card-body">
                                <p><?php the_field('fqa_content_1'); ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-heading">
                        <a data-toggle="collapse" data-target="#collapseTwo">
                         <?php the_field('faq_section_2'); ?>
                     </a>
                 </div>
                 <div id="collapseTwo" class="collapse" data-parent="#accordionExample">
                    <div class="card-body">
                        <p><?php the_field('faq_content_2'); ?><br>


                            <?php if ( have_rows( 'faq_points' ) ): ?>
                                <?php while( have_rows( 'faq_points' ) ): the_row();?>
                                   <i class="far fa-hand-point-right"></i><?php the_sub_field('cont_points'); ?><br>

                               <?php endwhile;?>
                           <?php endif;?>
                           



                           
                       </p>
                   </div>
               </div>
           </div>
           <div class="card">
            <div class="card-heading">
                <a data-toggle="collapse" data-target="#collapseThree">
                  <?php the_field('faq_section_3'); ?>
              </a>
          </div>
          <div id="collapseThree" class="collapse" data-parent="#accordionExample">
            <div class="card-body">
                <p><?php the_field('faq_content_3'); ?>
            </p>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-heading">
        <a data-toggle="collapse" data-target="#collapse4">
          <?php the_field('faq_section_4'); ?>
      </a>
  </div>
  <div id="collapse4" class="collapse" data-parent="#accordionExample">
    <div class="card-body">
        <p>S<?php the_field('faq_content_4'); ?>
    </p>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
<!-- Faq Section End -->


<?php get_footer();

?>