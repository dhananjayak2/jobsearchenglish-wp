<?php
/*Template Name: News  */
get_header();?>

    <!-- Blog Section Begin -->
    <section class="blog-section spad">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <div class="blog-details-inner">
                        <div class="blog-detail-title">
                            <h2>Newsletter</h2>

                        </div>
                    </div>
                    <div class="row">
<?php

        $args_cont_dig = array('order' => 'ASC', 'category' => '8', 'posts_per_page' => 12);

        ?>

        <?php $args_contdig_item = get_posts($args_cont_dig); ?>

        <?php if (have_posts()) : ?>

            <?php foreach ($args_contdig_item as $post) : setup_postdata($post); ?>



                <?php

                $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');

                ?>



                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="blog-item">
                                <div class="bi-pic">
                                    <img src="<?php echo $featured_img_url; ?>" alt="">
                                </div>
                                <div class="bi-text">
                                    <a href="<?php the_permalink() ?>">
                                        <h4><?php the_title(); ?></h4>
                                    </a>
                                    <p><?php echo substr(strip_tags($post->post_content), 0, 50); ?></p>
                                    <p><span> <?php echo date('M d, Y'); ?></span></p>
                                </div>
                            </div>
                        </div>


                  
<?php endforeach; ?>

            <?php wp_reset_postdata(); ?>

        <?php endif; ?>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Section End -->


  <?php get_footer();?>