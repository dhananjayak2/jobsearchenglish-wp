 <!-- Footer Section Begin -->
 <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer-left">
                        <div class="footer-logo">
                            <?php if ( is_active_sidebar( 'part2' ) ) : ?>
                        <?php dynamic_sidebar( 'part2' );
                            ?>
                        <?php endif;
                                ?>
                            
                        </div>
                        <?php if ( is_active_sidebar( 'part3' ) ) : ?>
                        <?php dynamic_sidebar( 'part3' );
                            ?>
                        <?php endif;
                                ?>

                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1">
                    <div class="footer-widget">
                        <h5>Quick Links </h5>
                       <?php

                wp_nav_menu(array('theme_location' => 'footer-menu', 
                             'menu_id' => FALSE,));

                ?>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="footer-widget">
                        <h5>Legal Stuff</h5>
                        <?php

                wp_nav_menu(array('theme_location' => 'footer-menu-2', 
                             'menu_id' => FALSE,));

                ?>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="newslatter-item">
                        <h5>Join Our Newsletter Now</h5>
                        <p>Get E-mail updates about our latest courese and special offers.</p>
                        <form action="#" class="subscribe-form">
                            <input type="text" placeholder="Enter Your Mail">
                            <button type="button">Subscribe</button>
                        </form>
                    </div>
                    <div class="footer-social">

                        <?php if ( is_active_sidebar( 'part4' ) ) : ?>
                        <?php dynamic_sidebar( 'part4' );
                            ?>
                        <?php endif;
                                ?>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-reserved">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright-text">
                            <!-- Link back to jobsearchenglish can't be removed. website is licensed under CC BY 3.0. -->
                            Copyright &copy;
                            <script>
                                document.write(new Date().getFullYear());
                            </script> All rights reserved | <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://jobsearchenglish.com" target="_blank">jobsearchenglish</a>
                            <!-- Link back to jobsearchenglish can't be removed. website is licensed under CC BY 3.0. -->
                        </div>
                        <div class="payment-pic">
                            <img src="<?php echo get_template_directory_uri() ?>/img/payment-method.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery-ui.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.countdown.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.nice-select.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.zoom.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.dd.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.slicknav.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/owl.carousel.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/main.js"></script>
</body>

</html>