<?php 
/* Template Name: news-single * Template Post Type: post*/ /*The template for displaying full width single posts. */
get_header(); ?>
<?php
           $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
           ?>
    <!-- Latest Blog Section Begin -->
    <section class="latest-blog spad">
        <div class="container">
            <div class="row">
                <img class="top-ad" src="<?php echo get_template_directory_uri() ?>/img/ad-top.png">
            </div>


        </div>
    </section>

    <section class="latest-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-md-12 all-quiz">

                    <div class="col-md-8 single-quiz">


                        <div class="quiz-bg-single">
                            <div class="quiz-topic ">
                                <h4><?php the_title(); ?></h4>
                            </div>
                            <div class="news-single">
                                <img src="<?php echo $featured_img_url; ?>">
                            </div>
                            <div class="queation">

                                <p class="queation-detail"><?php echo substr(strip_tags($post->post_content), 0, 10000);?>
                                </p>
                            </div>
                        </div>




                    </div>
                    <div class="col-md-4 side-ads "></div>
                    <img class="side-img " src="<?php echo get_template_directory_uri() ?>/img/side-ad-2.png ">
                </div>

            </div>
        </div>
    </section>


  <?php get_footer();?>